#!/bin/bash

set -ex

LIBDRM_VERSION="libdrm-2.4.110"

git clone https://gitlab.freedesktop.org/mesa/drm.git /drm
pushd /drm
git checkout $LIBDRM_VERSION

meson build \
   -D amdgpu=true \
   -D radeon=true \
   -D intel=false \
   -D nouveau=false \
   -D vmwgfx=false
ninja -C build install

rm -rf /drm
popd
