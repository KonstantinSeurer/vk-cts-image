#!/bin/bash

# Run Fossilize on some gens with RADV_FORCE_FAMILY to cover most of the
# potential compiler issues.

if [ $# != 2 ]; then
    echo "$0 <remote> <branch>"
    exit 1
fi

remote=$1
branch=$2

# Sienna Cichlid (GFX10.3)
./run-fossilize.sh -r $remote -b $branch -g sienna_cichlid

# Navi10 (GFX10)
./run-fossilize.sh -r $remote -b $branch -g navi10

# Vega10 (GFX9)
./run-fossilize.sh -r $remote -b $branch -g vega10

# Polaris10 (GFX8)
./run-fossilize.sh -r $remote -b $branch -g polaris10

# Hawaii (GFX7)
./run-fossilize.sh -r $remote -b $branch -g hawaii

# Pitcairn (GFX6)
./run-fossilize.sh -r $remote -b $branch -g pitcairn
