#!/bin/bash

if [ $# != 1 ]; then
    echo "$0 <gpu_family>"
    exit 1
fi

gpu_family=$1

./run-radv.sh -r origin -b main -g $gpu_family
./run-fossilize.sh -r origin -b main
